package ru.artemtsarev.gwt.shared;

import java.io.Serializable;
import java.util.List;

public class Folder implements Serializable {

    private boolean hasChildren;

    private int id;

    private String name;

    public Folder() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
