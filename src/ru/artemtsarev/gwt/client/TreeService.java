package ru.artemtsarev.gwt.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.artemtsarev.gwt.shared.Folder;

import java.util.ArrayList;


@RemoteServiceRelativePath("TreeService")
public interface TreeService extends RemoteService {

    ArrayList<Folder> readFolder(int id);

    Folder addFolder(String name, int parentId);

    ArrayList<Integer> deleteFolder(int id);

    Boolean renameFolder(String name, int id);

    Boolean replaceFolder(int id, int parentId);
}
