package ru.artemtsarev.gwt.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import ru.artemtsarev.gwt.client.TreeServiceAsync;
import ru.artemtsarev.gwt.client.event.AddFolderEvent;
import ru.artemtsarev.gwt.client.event.RenameFolderEvent;
import ru.artemtsarev.gwt.client.view.TreeView;
import ru.artemtsarev.gwt.shared.Folder;

import java.util.ArrayList;

public class TreePresenter implements TreeView.Presenter {

    private final TreeServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final TreeView view;

    private String strRootNotChange = "Данная операция для корневого каталога недоступна", strErrorReadFolder = "Произошла ошибка при чтении каталога",
            strErrorReplaceFolder = "Произошла ошибка при переносе каталога", strErrorDeleteFolder = "Произошла ошибка при удалении каталога",
            strFolderExist = "Каталог с указанным именем уже существует";

    public TreePresenter(TreeServiceAsync rpcService, HandlerManager eventBus, TreeView view) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.view = view;
        this.view.setPresenter(this);
    }

    public void go(final HasWidgets container) {
        container.add(view.asWidget());
    }

    @Override
    public void onAddFolderMenuItemClicked() {
        int id = view.getSelectedFolderId();
        eventBus.fireEvent(new AddFolderEvent(id));
        view.hidePopup();
    }

    @Override
    public void onReadFolderButtonClicked() {
        readFolder();
    }

    @Override
    public void onDeleteFolderMenuItemClicked() {
        deleteFolder();
        view.hidePopup();
    }

    @Override
    public void onRenameFolderMenuItemClicked() {
        int id = view.getSelectedFolderId();
        if (id != 0) {
            eventBus.fireEvent(new RenameFolderEvent(id));
        } else {
            Window.alert(strRootNotChange);
        }
        view.hidePopup();
    }

    @Override
    public void onReplaceFolder() {
        replaceFolder();
    }

    private void readFolder() {
        final int id = view.getSelectedFolderId();
        view.setLoading(id, true);
        rpcService.readFolder(id, new AsyncCallback<ArrayList<Folder>>() {
            @Override
            public void onFailure(Throwable caught) {
                view.setLoading(id, false);
                Window.alert(strErrorReadFolder);
            }

            @Override
            public void onSuccess(ArrayList<Folder> result) {
                for (Folder folder : result) {
                    view.addFolder(id, folder.getName(), folder.getId(), folder.isHasChildren());
                }
                view.setLoading(id, false);
            }
        });
    }

    private void deleteFolder() {
        final int id = view.getSelectedFolderId();
        if (id != 0) {
            rpcService.deleteFolder(id, new AsyncCallback<ArrayList<Integer>>() {
                public void onSuccess(ArrayList result) {
                    view.deleteFolders(result);
                }

                public void onFailure(Throwable caught) {
                    Window.alert(strErrorDeleteFolder);
                }

            });
        } else {
            Window.alert(strRootNotChange);
        }
    }

    private void replaceFolder() {
        final int idReplaceFolder = view.getIdReplaceFolder();
        final int idReplaceFolderTo = view.getIdReplaceFolderTo();
        rpcService.replaceFolder(idReplaceFolder, idReplaceFolderTo, new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
                Window.alert(strErrorReplaceFolder);
            }

            @Override
            public void onSuccess(Boolean result) {
                if (result) {
                    view.replaceFolder(idReplaceFolderTo, idReplaceFolder);
                } else {
                    Window.alert(strFolderExist);
                }
            }
        });
    }


}
