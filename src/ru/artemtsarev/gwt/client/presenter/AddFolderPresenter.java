package ru.artemtsarev.gwt.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.artemtsarev.gwt.client.TreeServiceAsync;
import ru.artemtsarev.gwt.client.view.AddFolderView;
import ru.artemtsarev.gwt.shared.Folder;

public class AddFolderPresenter implements MyDialogBoxPresenter {

    private final TreeServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final AddFolderView view;
    private final int parentId;
    private String strErrorAddFolder = "Невозможно добавить каталог", strEmptyName = "Имя каталога должно содержать хотя бы один символ",
                   strFolderExist = "Каталог с указанным именем уже существует";


    public AddFolderPresenter(TreeServiceAsync rpcService, HandlerManager eventBus, AddFolderView view, int parentId) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.view = view;
        this.view.setPresenter(this);
        this.parentId = parentId;
    }

    public void go() {
        view.showDialog();
    }

    @Override
    public void onOkButtonClicked() {

        String name = view.getFolderName();

        if(name.matches(".*\\s+.*")||name.isEmpty()) {
            Window.alert(strEmptyName);
        } else {
            rpcService.addFolder(name, parentId, new AsyncCallback<Folder>() {
                public void onSuccess(Folder result) {
                    if(result!=null) {
                        view.addNewFolder(parentId, result.getName(), result.getId(), result.isHasChildren());
                    } else {
                        Window.alert(strFolderExist);
                    }
                }

                public void onFailure(Throwable caught) {
                    Window.alert(strErrorAddFolder);
                }

            });
        }
    }

    @Override
    public void onCancelButtonClicked() {
        view.hideDialog();
    }
}
