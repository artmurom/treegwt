package ru.artemtsarev.gwt.client.presenter;

public interface MyDialogBoxPresenter {
    void onOkButtonClicked();

    void onCancelButtonClicked();
}
