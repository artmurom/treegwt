package ru.artemtsarev.gwt.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.artemtsarev.gwt.client.TreeServiceAsync;
import ru.artemtsarev.gwt.client.view.RenameFolderView;

public class RenameFolderPresenter implements MyDialogBoxPresenter {

    private final TreeServiceAsync rpcService;
    private final HandlerManager eventBus;
    private final RenameFolderView view;
    private final int id;
    private String strErrorRenameFolder = "Произошла ошибка при переименовании каталога", strEmptyName = "Имя каталога должно содержать хотя бы один символ",
            strFolderExist = "Каталог с указанным именем уже существует";

    public RenameFolderPresenter(TreeServiceAsync rpcService, HandlerManager eventBus, RenameFolderView view, int id) {
        this.rpcService = rpcService;
        this.eventBus = eventBus;
        this.view = view;
        this.view.setPresenter(this);
        this.id = id;
    }

    public void go() {
        view.showDialog();
    }

    @Override
    public void onOkButtonClicked() {

        final String name = view.getFolderName();

        if (name.matches(".*\\s+.*") || name.isEmpty()) {
            Window.alert(strEmptyName);
        } else {
            rpcService.renameFolder(name, id, new AsyncCallback<Boolean>() {

                public void onSuccess(Boolean result) {
                    if (result) {
                        view.renameFolder(name, id);
                    } else {
                        Window.alert(strFolderExist);
                    }
                }

                public void onFailure(Throwable caught) {
                    Window.alert(strErrorRenameFolder);
                }
            });
        }
    }

    @Override
    public void onCancelButtonClicked() {
        view.hideDialog();
    }
}
