package ru.artemtsarev.gwt.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.artemtsarev.gwt.shared.Folder;

import java.util.ArrayList;

public interface TreeServiceAsync {

    void addFolder(String name, int parentId, AsyncCallback<Folder> async);

    void renameFolder(String name, int id, AsyncCallback<Boolean> async);

    void replaceFolder(int id, int parentId, AsyncCallback<Boolean> async);

    void deleteFolder(int id, AsyncCallback<ArrayList<Integer>> async);

    void readFolder(int id, AsyncCallback<ArrayList<Folder>> async);
}
