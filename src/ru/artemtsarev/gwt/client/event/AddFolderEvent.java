package ru.artemtsarev.gwt.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddFolderEvent extends GwtEvent<AddFolderEventHandler> {
  public static Type<AddFolderEventHandler> TYPE = new Type<>();

  private int folderId;

  public AddFolderEvent(int folderId) {
    this.folderId = folderId;
  }

  public int getFolderId() {
    return folderId;
  }

  @Override
  public Type<AddFolderEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(AddFolderEventHandler handler) {
    handler.onNewFolder(this);
  }
}
