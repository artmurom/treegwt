package ru.artemtsarev.gwt.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RenameFolderEvent extends GwtEvent<RenameFolderEventHandler>{
  public static Type<RenameFolderEventHandler> TYPE = new Type<>();

  private int folderId;

  public RenameFolderEvent(int folderId) {
    this.folderId = folderId;
  }

  public int getFolderId() {
    return folderId;
  }
  
  @Override
  public Type<RenameFolderEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(RenameFolderEventHandler handler) {
    handler.onRenameFolder(this);
  }
}
