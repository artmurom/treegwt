package ru.artemtsarev.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RenameFolderEventHandler extends EventHandler {
  void onRenameFolder(RenameFolderEvent event);
}
