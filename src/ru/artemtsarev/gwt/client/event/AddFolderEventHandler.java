package ru.artemtsarev.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddFolderEventHandler extends EventHandler {
  void onNewFolder(AddFolderEvent event);
}
