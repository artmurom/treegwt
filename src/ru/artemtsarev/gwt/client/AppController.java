package ru.artemtsarev.gwt.client;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import ru.artemtsarev.gwt.client.event.AddFolderEvent;
import ru.artemtsarev.gwt.client.event.AddFolderEventHandler;
import ru.artemtsarev.gwt.client.event.RenameFolderEvent;
import ru.artemtsarev.gwt.client.event.RenameFolderEventHandler;
import ru.artemtsarev.gwt.client.presenter.AddFolderPresenter;
import ru.artemtsarev.gwt.client.presenter.RenameFolderPresenter;
import ru.artemtsarev.gwt.client.presenter.TreePresenter;
import ru.artemtsarev.gwt.client.view.AddFolderViewImpl;
import ru.artemtsarev.gwt.client.view.RenameFolderViewImpl;
import ru.artemtsarev.gwt.client.view.TreeViewImpl;

public class AppController {
    private final HandlerManager eventBus;
    private final TreeServiceAsync rpcService;
    private HasWidgets container;
    private TreeViewImpl treeViewImpl;

    public AppController(TreeServiceAsync rpcService, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.rpcService = rpcService;
        bind();
    }

    private void bind() {

        eventBus.addHandler(AddFolderEvent.TYPE, new AddFolderEventHandler() {
            @Override
            public void onNewFolder(AddFolderEvent event) {
                doAddNewFolder(event.getFolderId());
            }
        });

        eventBus.addHandler(RenameFolderEvent.TYPE, new RenameFolderEventHandler() {
            @Override
            public void onRenameFolder(RenameFolderEvent event) {
                doRenameFolder(event.getFolderId());
            }
        });

    }

    private void doAddNewFolder(int id) {
        AddFolderPresenter presenter = new AddFolderPresenter(rpcService, eventBus, new AddFolderViewImpl(treeViewImpl), id);
        presenter.go();
    }

    private void doRenameFolder(int id) {
        RenameFolderPresenter presenter = new RenameFolderPresenter(rpcService, eventBus, new RenameFolderViewImpl(treeViewImpl), id);
        presenter.go();
    }

    public void go(final HasWidgets container) {
        this.container = container;

        treeViewImpl = new TreeViewImpl();

        TreePresenter presenter = new TreePresenter(rpcService, eventBus, treeViewImpl);

        presenter.go(container);
    }
}
