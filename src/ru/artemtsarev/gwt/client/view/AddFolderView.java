package ru.artemtsarev.gwt.client.view;

import ru.artemtsarev.gwt.client.presenter.MyDialogBoxPresenter;

public interface AddFolderView {

    void setPresenter(MyDialogBoxPresenter presenter);

    String getFolderName();

    void addNewFolder(int parentId, String name, int id, boolean hasChildren);

    void showDialog();

    void hideDialog();
}

