package ru.artemtsarev.gwt.client.view;

import ru.artemtsarev.gwt.client.presenter.MyDialogBoxPresenter;

public class AddFolderViewImpl extends MyDialogBoxView implements AddFolderView {

    private static final String TITLE = "Введите имя каталога";

    private TreeViewImpl parent;

    public AddFolderViewImpl(TreeViewImpl treeViewImpl) {
        super(TITLE);
        parent = treeViewImpl;
    }

    public void hideDialog() {
        super.hideDialog();
    }

    @Override
    public void setPresenter(MyDialogBoxPresenter presenter) {
        super.setPresenter(presenter);
    }

    @Override
    public String getFolderName() {
        return super.getFolderName();
    }

    public void addNewFolder(int parentId, String name, int id, boolean hasChildren) {
        parent.addNewFolder(parentId, name, id, hasChildren);
        hideDialog();
    }

    @Override
    public void showDialog() {
        super.showDialog();
    }

}