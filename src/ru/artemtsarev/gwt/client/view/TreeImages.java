package ru.artemtsarev.gwt.client.view;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Tree;

public class TreeImages implements Tree.Resources {
    @Override
    public ImageResource treeClosed() {
        // TODO Auto-generated method stub
        return MyResources.INSTANCE.leaf();
    }

    @Override
    public ImageResource treeLeaf() {
        // TODO Auto-generated method stub
        return MyResources.INSTANCE.leaf();
    }

    @Override
    public ImageResource treeOpen() {
        // TODO Auto-generated method stub
        return MyResources.INSTANCE.leaf();
    }

    interface MyResources extends ClientBundle {
        public static final MyResources INSTANCE = GWT.create(MyResources.class);

        @Source("image/none.png")
        public ImageResource leaf();
    }
}