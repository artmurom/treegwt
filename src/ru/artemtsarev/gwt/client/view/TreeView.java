package ru.artemtsarev.gwt.client.view;

import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;

public interface TreeView {

    void setPresenter(Presenter presenter);

    void addFolder(int parentId, String name, int id, boolean hasChildren);

    void replaceFolder(int parentId, int id);

    int getIdReplaceFolder();

    int getIdReplaceFolderTo();

    void hidePopup();

    void setLoading(int id, boolean loading);

    void deleteFolders(ArrayList<Integer> listId);

    int getSelectedFolderId();

    Widget asWidget();

    public interface Presenter {
        void onAddFolderMenuItemClicked();

        void onReadFolderButtonClicked();

        void onDeleteFolderMenuItemClicked();

        void onRenameFolderMenuItemClicked();

        void onReplaceFolder();
    }

}

