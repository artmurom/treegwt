package ru.artemtsarev.gwt.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import ru.artemtsarev.gwt.client.presenter.MyDialogBoxPresenter;

public abstract class MyDialogBoxView extends DialogBox {

    private MyDialogBoxPresenter presenter;

    private VerticalPanel verticalPanel;
    private HorizontalPanel horizontalPanel;

    private TextBox tbFolderName;
    private Button btnOk, btnCancel;

    private String strBtnOk = "Ок", strBtnCancel = "Отмена";

    public MyDialogBoxView(String name) {

        setText(name);
        setAnimationEnabled(true);
        setGlassEnabled(false);

        verticalPanel = new VerticalPanel();

        horizontalPanel = new HorizontalPanel();

        tbFolderName = new TextBox();
        tbFolderName.setVisibleLength(40);
        tbFolderName.setMaxLength(40);

        btnOk = new Button(strBtnOk);
        btnOk.addClickHandler(new OkClickHandler());
        btnCancel = new Button(strBtnCancel);
        btnCancel.addClickHandler(new CancelClickHandler());
        btnOk.setStyleName("my-Button");
        btnCancel.setStyleName("my-Button");

        horizontalPanel.add(btnOk);
        horizontalPanel.add(btnCancel);

        verticalPanel.add(tbFolderName);
        verticalPanel.add(horizontalPanel);

       center();
       setAutoHideEnabled(true);
        setWidget(verticalPanel);
    }

    public void hideDialog() {
        hide();
    }

    public void setPresenter(MyDialogBoxPresenter presenter) {
        this.presenter = presenter;
    }

    public String getFolderName() {
        return tbFolderName.getText();
    }

    public void showDialog() {
        show();
    }

    class OkClickHandler implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            presenter.onOkButtonClicked();
        }
    }

    class CancelClickHandler implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            presenter.onCancelButtonClicked();
        }
    }

}