package ru.artemtsarev.gwt.client.view;

import ru.artemtsarev.gwt.client.presenter.MyDialogBoxPresenter;

public interface RenameFolderView {

    void setPresenter(MyDialogBoxPresenter presenter);

    String getFolderName();

    void renameFolder(String name, int id);

    void showDialog();

    void hideDialog();
}

