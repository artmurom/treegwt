package ru.artemtsarev.gwt.client.view;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.*;

import java.util.*;


public class TreeViewImpl extends Composite implements TreeView {


    private Presenter presenter;

    private HashMap<Integer, MyTreeItem> folders;

    private Tree tree;
    private Label selectedFolder = new Label();
    private PopupPanel popupPanel;
    private MenuBar contextMenu;
    private MenuItem addFolder, renameFolder, deleteFolder;

    private String strRoot = "ROOT", strNewFolder = "Новая папка", strRenameFolder = "Переименовать папку", strRemoveFolder = "Удалить папку";

    private int idReplaceFolder, idReplaceFolderTo;


    public TreeViewImpl() {

        DecoratorPanel contentTableDecorator = new DecoratorPanel();
        initWidget(contentTableDecorator);

        folders = new HashMap<>();

        popupPanel = new PopupPanel(true);

        contextMenu = new MenuBar(true);

        addFolder = new MenuItem(new SafeHtmlBuilder().appendEscaped(strNewFolder).toSafeHtml());
        renameFolder = new MenuItem(new SafeHtmlBuilder().appendEscaped(strRenameFolder).toSafeHtml());
        deleteFolder = new MenuItem(new SafeHtmlBuilder().appendEscaped(strRemoveFolder).toSafeHtml());

        addFolder.setStyleName("MenuItem");
        renameFolder.setStyleName("MenuItem");
        deleteFolder.setStyleName("MenuItem");

        addFolder.setScheduledCommand(new AddFolderScheduler());
        renameFolder.setScheduledCommand(new RenameFolderScheduler());
        deleteFolder.setScheduledCommand(new DeleteFolderScheduler());

        contextMenu.addItem(addFolder);
        contextMenu.addItem(renameFolder);
        contextMenu.addItem(deleteFolder);

        popupPanel.add(contextMenu);

        TreeImages tx = new TreeImages();
        tree = new Tree(tx, false);

        MyTreeItem root = new MyTreeItem(tree, strRoot, true, false);

        folders.put(0, root);

        tree.addItem(root);
        tree.setSelectedItem(root);

        contentTableDecorator.add(tree);
    }


    @Override
    public void addFolder(int parentId, String name, int id, boolean hasChildren) {
        MyTreeItem parent = folders.get(parentId);
        MyTreeItem treeItem = new MyTreeItem(tree, name, hasChildren, true);
        parent.showIndicator();
        parent.addItem(treeItem);
        folders.put(id, treeItem);
    }

    public void addNewFolder(int parentId, String name, int id, boolean hasChildren) {
        MyTreeItem parent = folders.get(parentId);
        if (parent.isLoaded()) {
            addFolder(parentId, name, id, hasChildren);
            parent.setState(true);
        } else {
            tree.setSelectedItem(parent);
            presenter.onReadFolderButtonClicked();
        }
    }

    @Override
    public void replaceFolder(int parentId, int id) {
        MyTreeItem oldParent = (MyTreeItem) folders.get(id).getParentItem();
        TreeItem treeItem = folders.get(id);
        oldParent.removeItem(treeItem);
        MyTreeItem newParent = folders.get(parentId);
        if (oldParent.getChildCount() == 0) {
            oldParent.hideIndicator();
        }
        if (newParent.isLoaded() || !newParent.hasChildren()) {
            newParent.addItem(treeItem);
            newParent.showIndicator();
            newParent.setState(true);
        } else {
            treeItem.remove();
            folders.remove(treeItem);
            tree.setSelectedItem(newParent);
            presenter.onReadFolderButtonClicked();
        }
    }

    public void renameFolder(String name, int id) {
        folders.get(id).setName(name);
    }

    @Override
    public void setLoading(int id, boolean loading) {
        MyTreeItem treeItem = folders.get(id);
        treeItem.setLoading(loading);
    }

    @Override
    public int getSelectedFolderId() {
        MyTreeItem value = (MyTreeItem) tree.getSelectedItem();
        return getFolderId(value);
    }

    private int getFolderId(MyTreeItem treeItem) {
        int key = 0;
        for (Map.Entry<Integer, MyTreeItem> entry : folders.entrySet()) {
            if (entry.getValue().equals(treeItem)) {
                key = entry.getKey();
            }
        }
        return key;
    }

    @Override
    public void deleteFolders(ArrayList<Integer> listId) {
        MyTreeItem child = folders.get(listId.get(0));
        MyTreeItem parent = (MyTreeItem) child.getParentItem();
        for (Integer id : listId) {
            MyTreeItem treeItem = folders.get(id);
            treeItem.remove();
            folders.remove(id);
        }
        if (parent.getChildCount() == 0) {
            parent.hideIndicator();
        }
    }

    private void changeSelectedFolder(Object object) {
        Label newSelectedFolder = (Label) object;
        selectedFolder.setStyleName("Folder");
        selectedFolder = newSelectedFolder;
        newSelectedFolder.setStyleName("Folder-selected");
    }

    private void showPopup(int positionX, int positionY) {
        popupPanel.setPopupPosition(positionX, positionY);
        popupPanel.show();
    }

    @Override
    public void hidePopup() {
        popupPanel.hide();
    }

   void queryReplaceFolder(TreeItem source, TreeItem target) {
        idReplaceFolder = getFolderId((MyTreeItem) source);
        idReplaceFolderTo = getFolderId((MyTreeItem) target);
        presenter.onReplaceFolder();
    }

    @Override
    public int getIdReplaceFolder() {
        return idReplaceFolder;
    }

    @Override
    public int getIdReplaceFolderTo() {
        return idReplaceFolderTo;
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    class AddFolderScheduler implements Scheduler.ScheduledCommand {
        @Override
        public void execute() {
            presenter.onAddFolderMenuItemClicked();
        }
    }

    class DeleteFolderScheduler implements Scheduler.ScheduledCommand {
        @Override
        public void execute() {
            presenter.onDeleteFolderMenuItemClicked();
        }
    }

    class RenameFolderScheduler implements Scheduler.ScheduledCommand {
        @Override
        public void execute() {
            presenter.onRenameFolderMenuItemClicked();
        }
    }


    class MyTreeItem extends TreeItem implements ContextMenuHandler, ClickHandler {

        private Tree tree;
        private Button indicator;
        private DragDropLabel dragDropLabel;

        private HandlerRegistration handlerContext, handlerClick;

        private boolean hasChildren, draggable, loaded;

        MyTreeItem(Tree tree, String name, boolean hasChildren, boolean draggable) {
            super();

            this.tree = tree;
            this.hasChildren = hasChildren;
            this.draggable = draggable;

            HorizontalPanel hp = new HorizontalPanel();

            indicator = new Button("1");
            if (!hasChildren) {
                hideIndicator();
                loaded = true;
            } else {
                indicator.setStyleName("Indicator-close");
            }
            indicator.addClickHandler(this);

            dragDropLabel = new DragDropLabel(TreeViewImpl.this, name, draggable, true);

            dragDropLabel.setStyleName("Folder");
            dragDropLabel.sinkEvents(Event.ONCONTEXTMENU);
            handlerContext = dragDropLabel.addDomHandler(this, ContextMenuEvent.getType());
            dragDropLabel.sinkEvents(Event.BUTTON_LEFT);
            handlerClick = dragDropLabel.addClickHandler(this);

            hp.add(indicator);
            hp.add(dragDropLabel);
            setWidget(hp);
        }

        public void setName(String name) {
            dragDropLabel.setText(name);
        }

        public void onContextMenu(ContextMenuEvent event) {
            event.preventDefault();
            event.stopPropagation();

            tree.setSelectedItem(this);

            int positionX = event.getNativeEvent().getClientX() /*- event.getRelativeElement().getAbsoluteLeft()*/;
            int positionY = event.getNativeEvent().getClientY();

            changeSelectedFolder(event.getSource());
            showPopup(positionX, positionY);
        }

        void setLoading(boolean loading) {
            if (loading) {
                indicator.setEnabled(false);
                dragDropLabel.setStyleName("Folder-load");
                dragDropLabel.setDragDrop(false);
                handlerContext.removeHandler();
                handlerClick.removeHandler();
            } else {
                indicator.setEnabled(true);
                setState(true);
                dragDropLabel.setStyleName("Folder");
                loaded = true;
                dragDropLabel.setDragDrop(true);
                dragDropLabel.addDomHandler(this, ContextMenuEvent.getType());
                dragDropLabel.addClickHandler(this);
            }
        }

       boolean isLoaded() {
            return loaded;
        }

        boolean hasChildren() {
            return hasChildren;
        }

        void hideIndicator() {
            indicator.setStyleName("Indicator");
            indicator.setEnabled(false);
            hasChildren = false;
        }

        void showIndicator() {
            indicator.setStyleName("Indicator-close");
            indicator.setEnabled(true);
            hasChildren = true;
        }

        @Override
        public void setState(boolean open) {
            super.setState(open);
            if (open) {
                indicator.setStyleName("Indicator-open");
            } else {
                indicator.setStyleName("Indicator-close");
            }
        }

        @Override
        public void onClick(ClickEvent event) {
            tree.setSelectedItem(this);
            if (event.getSource() instanceof Button) {
                if (this.getChildCount() == 0 && hasChildren) {
                    presenter.onReadFolderButtonClicked();
                } else {
                    if (getState()) {
                        setState(false);
                    } else {
                        setState(true);
                    }
                }
            } else if (event.getSource() instanceof Label) {
                changeSelectedFolder(event.getSource());
            }

        }
    }
}

class DragDropLabel extends Label {

    private static DragDropLabel dragging = null;
    private final boolean droppable, draggable;
    private TreeViewImpl view;
    private HandlerRegistration handlerDragStart, handlerDragOver, handlerDrop;

    DragDropLabel(TreeViewImpl view, String text, boolean draggable, boolean droppable) {
        super(text);
        if (draggable) {
            initDrag();
        }
        if (droppable) {
            initDrop();
        }
        this.view = view;
        this.draggable = draggable;
        this.droppable = droppable;
    }

    private void initDrag() {
        getElement().setDraggable(Element.DRAGGABLE_TRUE);
        handlerDragStart = addDragStartHandler(new DragStartHandler() {
            @Override
            public void onDragStart(DragStartEvent event) {
                dragging = DragDropLabel.this;
                event.setData("ID", "UniqueIdentifier");
                event.getDataTransfer().setDragImage(getElement(), 10, 10);
            }
        });
    }


    void setDragDrop(boolean droppable) {
        if(!droppable) {
            if (draggable) {
                handlerDragStart.removeHandler();
            }
            handlerDragOver.removeHandler();
            handlerDrop.removeHandler();
        } else {
            if (draggable) {
                initDrag();
            }
            initDrop();
        }
    }

    private void initDrop() {
        handlerDragOver = addDomHandler(new DragOverHandler() {
            @Override
            public void onDragOver(DragOverEvent event) {
            }
        }, DragOverEvent.getType());

        handlerDrop = addDomHandler(new DropHandler() {
            @Override
            public void onDrop(DropEvent event) {
                event.preventDefault();
                if (dragging != null) {
                    TreeItem target = null;
                    TreeItem source = null;
                    Tree tree = (Tree) DragDropLabel.this.getParent().getParent();
                    List<TreeItem> treeItems = new ArrayList<>();
                    treeItems.add(tree.getItem(0));
                    while (!treeItems.isEmpty()) {
                        TreeItem item = treeItems.remove(0);
                        for (int i = 0; i < item.getChildCount(); i++) {
                            treeItems.add(item.getChild(i));
                        }

                        Iterator<Widget> widgets = ((HorizontalPanel) item.getWidget()).iterator();
                        widgets.next();
                        Widget widget = widgets.next();

                        if (widget != null) {
                            if (widget == dragging) {
                                source = item;
                                if (target != null) {
                                    break;
                                }
                            }
                            if (widget == DragDropLabel.this) {
                                target = item;
                                if (source != null) {
                                    break;
                                }
                            }
                        }
                    }

                    if (source != null && target != null) {
                        TreeItem testTarget = target;
                        while (testTarget != null) {
                            if (testTarget == source) {
                                return;
                            }
                            testTarget = testTarget.getParentItem();
                        }
                        view.queryReplaceFolder(source, target);
                    }
                    dragging = null;
                }
            }
        }, DropEvent.getType());
    }


}

