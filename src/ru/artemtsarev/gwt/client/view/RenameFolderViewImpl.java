package ru.artemtsarev.gwt.client.view;

import ru.artemtsarev.gwt.client.presenter.MyDialogBoxPresenter;

public class RenameFolderViewImpl extends MyDialogBoxView implements RenameFolderView {

    private static final String TITLE = "Введите новое имя каталога";

    private TreeViewImpl parent;

    public RenameFolderViewImpl(TreeViewImpl treeViewImpl) {
        super(TITLE);
        parent = treeViewImpl;
    }

    public void hideDialog() {
        super.hideDialog();
    }

    @Override
    public void setPresenter(MyDialogBoxPresenter presenter) {
        super.setPresenter(presenter);
    }

    @Override
    public String getFolderName() {
        return super.getFolderName();
    }


    @Override
    public void renameFolder(String name, int id) {
        parent.renameFolder(name, id);
        hideDialog();
    }

    @Override
    public void showDialog() {
        super.showDialog();
    }

}