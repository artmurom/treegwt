package ru.artemtsarev.gwt.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.artemtsarev.gwt.client.TreeService;
import ru.artemtsarev.gwt.shared.Folder;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TreeServiceImpl extends RemoteServiceServlet implements TreeService {

    private static final String TABLE = "folders";
    private static final String ID = "id";
    private static final String PARENT_ID = "parent_id";
    private static final String NAME = "name";

    @Override
    public ArrayList<Folder> readFolder(int id) {

        Connection conn = createConnection();

        ArrayList<Folder> folders = new ArrayList<>();

        try (Statement stm = conn.createStatement()) {
            String query = "SELECT * FROM " + TABLE + " WHERE " + PARENT_ID + " = '" + id + "' ORDER BY " + NAME + ";";
            ResultSet rs = stm.executeQuery(query);
            while (rs.next()) {
                Folder folder = new Folder();

                folder.setName(rs.getString(NAME));
                folder.setId(rs.getInt(ID));

                folders.add(folder);
            }
            for (Folder folder : folders) {
                String query2 = "SELECT * FROM " + TABLE + " WHERE " + PARENT_ID + " = '" + folder.getId() + "';";
                ResultSet rs2 = stm.executeQuery(query2);
                if (!rs2.next()) {
                    folder.setHasChildren(false);
                } else {
                    folder.setHasChildren(true);
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            closeConnection(conn);
        }

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return folders;
    }

    @Override
    public Folder addFolder(String name, int parentId) {

        Connection conn = createConnection();

        Folder folder = null;

        try (Statement stm = conn.createStatement()) {
            String query1 = "SELECT " + NAME + " FROM " + TABLE + " WHERE " + PARENT_ID + " = '" + parentId + "' AND " + NAME + " = '" + name + "';";
            ResultSet rs1 = stm.executeQuery(query1);
            if (rs1.next()) {
                return folder;
            }

            String query2 = "SELECT " + ID + " FROM " + TABLE + " ORDER BY " + ID + " DESC LIMIT 1";
            ResultSet rs2 = stm.executeQuery(query2);
            rs2.next();
            int id = rs2.getInt(ID) + 1;

            String queryUpdate = "INSERT INTO " + TABLE + " (" + ID + ", " + PARENT_ID + ", " + NAME + ") VALUES ('" + id + "', '" + parentId + "', '" + name + "');";
            stm.executeUpdate(queryUpdate);

            folder = new Folder();
            folder.setId(id);
            folder.setName(name);
            folder.setHasChildren(false);

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return folder;
    }

    @Override
    public Boolean renameFolder(String name, int id) {

        Connection conn = createConnection();

        try (Statement stm = conn.createStatement()) {
            String query = "SELECT * FROM " + TABLE + " WHERE " + NAME + " = '" + name + "' AND " + PARENT_ID + " IN (SELECT " + PARENT_ID + " FROM " + TABLE + " WHERE " + ID + " = '" + id + "');";
            ResultSet rs = stm.executeQuery(query);
            if (rs.next()) {
                return false;
            }

            String queryUpdate = "UPDATE " + TABLE + " SET " + NAME + " = '" + name + "' WHERE " + ID + " = " + id + ";";
            stm.executeUpdate(queryUpdate);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            closeConnection(conn);
        }
        return true;
    }

    @Override
    public Boolean replaceFolder(int id, int parentId) {
        Connection conn = createConnection();

        try (Statement stm = conn.createStatement()) {
            String query = "SELECT * FROM " + TABLE + " WHERE " + ID + " = '" + id + "' AND " + NAME + " IN (SELECT " + NAME + " FROM " + TABLE + " WHERE " + PARENT_ID + " = '" + parentId + "');";
            ResultSet rs = stm.executeQuery(query);
            if (rs.next()) {
                return false;
            }

            String queryUpdate = "UPDATE " + TABLE + " SET " + PARENT_ID + " = '" + parentId + "' WHERE " + ID + " = " + id + ";";
            stm.executeUpdate(queryUpdate);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            closeConnection(conn);
        }
        return true;
    }

    @Override
    public ArrayList<Integer> deleteFolder(int id) {

        Connection conn = createConnection();

        ArrayList<Integer> listId = new ArrayList<>();

        try {
            deleteFolderRecursive(conn, id, listId);
        } finally {
            closeConnection(conn);
        }
        return listId;
    }

    private void deleteFolderRecursive(Connection conn, int id, ArrayList<Integer> listId) {
        try (Statement stm = conn.createStatement()) {
            String query = "DELETE FROM " + TABLE + " WHERE " + ID + " = '" + id + "';";
            stm.executeUpdate(query);
            listId.add(id);
            String query2 = "SELECT * FROM " + TABLE + " WHERE " + PARENT_ID + " = '" + id + "';";
            ResultSet rs2 = stm.executeQuery(query2);
            while (rs2.next()) {
                int id2 = rs2.getInt(ID);
                deleteFolderRecursive(conn, id2, listId);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    private Connection createConnection() {
        Context initialContext = null;
        try {
            initialContext = new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }

        Context environmentContext = null;
        try {
            environmentContext = (Context) initialContext
                    .lookup("java:comp/env");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        String dataResourceName = "jdbc/TreeGWT";

        DataSource dataSource = null;
        try {
            dataSource = (DataSource) environmentContext
                    .lookup(dataResourceName);
        } catch (NamingException e) {
            e.printStackTrace();
        }

        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    private void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}





